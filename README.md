# Sbproject [Done]

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

#Backend and Database connection
The database we have used is mongo db.
There is a separate file for backend code. Open it.
1.Run 'npm install' command in cmd to install all the required packages.
2.Run 'node server.js' to run the server and to make connection to the database.
3.After connection is made, open browser and paste 'http://localhost:3000/AddBooks' and press enter.
This will add the books data in the database and now can close this tab.

#Angular side
1.Run 'npm install' command in cmd to install all the required packages.
2.Run `ng serve` and Navigate to `http://localhost:4200/`.


We have assumed that user the already logged in so we have not created any login and sign up page nor it was mentioned in the assignment pdf.
So, we have not passed any user id and JWT(JSON WEB TOKEN) in the api's. We have directly stored the cart and orders data in database.